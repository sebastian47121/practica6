#include "bola.h"
#include <math.h>
#include <random>
#include <time.h>

bola::bola()
{
    ax=0;
    ay=0;
    e=1;
    k=0.01;
    dt=0.1;
    g=10;
    vx=rand()%300;
    vy=rand()%300;
    masa=rand()%600+50;
    radio=rand()%30 +5;
    v=sqrt(pow(vx,2)+pow(vy,2));
    angulo= atan2(vy,vx);
    srand(time(NULL));


}

QRectF bola::boundingRect() const
{
    return QRectF(-10,-10,20,20);
}

void bola::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
//    painter->setBrush(Qt::red);
//    painter->drawEllipse(boundingRect());
    QPixmap pixmap;
    pixmap.load(":/masterball.png");
    painter->drawPixmap(boundingRect(),pixmap,pixmap.rect());
}
void bola::mover()
{
    ax=-(((k*pow(v,2)*pow(radio,2))/masa)*cos(angulo));
    ay=-(((k*pow(v,2)*pow(radio,2))/masa)*sin(angulo))-g;
    vx=vx+ax*dt;
    vy=vy+ay*dt;
    //double posX=x(),posY=y();
    //if (posY>0)
    setPos(x()+vx*dt+((ax*pow(dt,2))/2),y()-vy*dt+((ay*pow(dt,2))/2));
    /*else{
        posY=posY-vy*dt+((ay*pow(dt,2))/2);
        posX=0;
        setPos(posX,posY);
    }*/
    angulo=atan2(vy,vx);
    v= sqrt(pow(vx,2)+pow(vy,2));
}

void bola::choque()
{
    v=-v;
    vx=vx/1.09;
    vy=-vy/1.3;

}

void bola::choquePared(){
    v=-v;
    vx=-vx/1.1;
}

