#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

/*  Este programa genera bolas o pelotas ilimitadas al hacer click en una posicion y generando la bola en dicha posicion
    cada bola tiene valores diferentes los cuales serán aleatorios, y estos datos van a variar la forma de moverse dichas bolas
    cada bola será afectada por las físicas*/


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene=new QGraphicsScene(0,0,1000,500);
    ui->graphicsView->setScene(scene);


    ui->graphicsView->setBackgroundBrush(QImage(":/bg2.jpg"));

    timer=new QTimer(this);
    timer->stop();
    connect(timer,SIGNAL(timeout()),this,SLOT(animar()));
    timer->start(15);

    l1=new QGraphicsLineItem(0,0,1000,0); //Linea arriba
    l2=new QGraphicsLineItem(0,0,0,500);    //Linea Izquierda
    l3=new QGraphicsLineItem(1000,0,1000,500);  //Linea derecha
    l4=new QGraphicsLineItem(0,500,1000,500);   //Linea abajo
    scene->addItem(l1);
    scene->addItem(l2);
    scene->addItem(l3);
    scene->addItem(l4);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete scene;

}
void MainWindow::animar()
{
    for(int i=0; i<bolas.length();i++)      //For que itera la bolas existentes
    {
        bolas.at(i)->mover();       //Mueve dichas bolas
        if(!bolas.at(i)->collidingItems().empty())      //Si se encuentran colisionando
        {
            bolas.at(i)->choque();      //Realizan el choque normal
            if(bolas.at(i)->collidesWithItem(l2)|| bolas.at(i)->collidesWithItem(l3))   //Si colisionan con las paredes
                bolas.at(i)->choquePared();     //Realizan el choque normal hacia una pared

        }


}
}


void MainWindow::mousePressEvent(QMouseEvent *ev)
{
    bolas.append(new bola());       //Agrega una bola a la lista de Bolas
    scene->addItem(bolas.last());       //Agrega dicha bola a la escena
    bolas.last()->setPos(ev->x(),ev->y());  //Agrega dicha bola en la posicion del mouse

}
