#ifndef BOLA_H
#define BOLA_H
#include <QGraphicsItem>
#include <QPainter>

class bola: public QGraphicsItem
{
public:
    bola();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mover();
    void choque();
    void choquePared();

    double ax, ay, e, k, dt, g, vx, vy, masa, radio, v, angulo;
};

#endif // BOLA_H
