#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsLineItem>
#include <QKeyEvent>
#include <QGraphicsView>
#include <QTimer>
#include "bola.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow();
public slots:
    void animar();

private:
    Ui::MainWindow *ui;
    QGraphicsScene* scene;

    QGraphicsLineItem* l1;
    QGraphicsLineItem* l2;
    QGraphicsLineItem* l3;
    QGraphicsLineItem* l4;
    bool flag=true;

    QList<bola*> bolas;

    QTimer *timer;


    void mousePressEvent(QMouseEvent *ev);

};

#endif // MAINWINDOW_H
